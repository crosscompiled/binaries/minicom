FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y git
WORKDIR /
RUN git clone https://salsa.debian.org/minicom-team/minicom.git
WORKDIR /minicom
RUN git checkout v2.8
RUN apt-get install -y automake gettext gcc pkg-config make libncurses-dev
RUN ./autogen.sh
RUN ./configure
RUN command
RUN make
ENTRYPOINT [ "/bin/bash" ]